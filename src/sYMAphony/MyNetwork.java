package sYMAphony;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class MyNetwork 
{
	public int size;
	public int[][] adjacenceGraph;
	public int[] edgeCount;
	public int[][] lineGraph;
	
	public boolean forcePos = false;
	public int[] posX;
	public int[] posY;

	public MyNetwork(int parSize)
	{
		size = parSize;
		adjacenceGraph = new int[size][size];
		for (int i = 0; i < size; ++i)
			for (int j = 0; j < size; j++)  
				adjacenceGraph[i][j] = -1;
		edgeCount = new int[size];
		for (int i = 0; i < size; i++)
			edgeCount[i] = 0;
		lineGraph = new int[size][size];
		for (int i = 0; i < size; ++i)
			for (int j = 0; j < size; j++)  
				lineGraph[i][j] = -1;
		posX = new int[size];
		for (int i = 0; i < size; i++)
			posX[i] = 0;
		posY = new int[size];
		for (int i = 0; i < size; i++)
			posY[i] = 0;
	}
	
	public void RandomFill(int linkNumber, int minTime, int maxTime)
	{
		for (int i = 0; i < size; ++i)
			for (int j = 0; j <= i; j++)  
				adjacenceGraph[i][j] = -1;
		int filled = 0;
		Random generator = new Random();
		linkNumber = Math.min(linkNumber, size * size);
		while (filled < linkNumber)
		{
			int i = Math.abs(generator.nextInt()) % (size - 1) + 1;
			int j = Math.abs(generator.nextInt()) % i;
			if (adjacenceGraph[i][j] == -1)
			{
				int time = generator.nextInt() % (maxTime - minTime + 1) + minTime;
				adjacenceGraph[i][j] = time;
				filled++;
			}
		}
		
		for (int i = 0; i < size; i++)
			edgeCount[i] = 0;
		for (int i = 0; i < size; ++i)
			for (int j = 0; j <= i; j++)
				if (adjacenceGraph[i][j] != -1)
				{
					edgeCount[i] += 1;
					edgeCount[j] += 1;
				}
	}
	
	public boolean IsConnex()
	{
		ArrayList<Integer> connected = new ArrayList<Integer>();
		Stack<Integer> newlyAdded = new Stack<Integer>();
		connected.add(0);
		newlyAdded.push(0);
		while (newlyAdded.size() > 0)
		{
			int toCheck = newlyAdded.pop();
			for (int i = 0; i <= toCheck; ++i)
				if (adjacenceGraph[toCheck][i] != -1)
				{
					if (!connected.contains(i))
					{
						connected.add(i);
						newlyAdded.push(i);
					}
				}
			for (int i = toCheck + 1; i < size; ++i)
				if (adjacenceGraph[i][toCheck] != -1)
				{
					if (!connected.contains(i))
					{
						connected.add(i);
						newlyAdded.push(i);
					}
				}
		}
		return (connected.size() == size);
	}
}
