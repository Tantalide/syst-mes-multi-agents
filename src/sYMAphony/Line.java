package sYMAphony;

import java.util.ArrayList;

public class Line
{
	final int id_;
	int vehicleCount_;
	int averageWaitDelay_;
	ArrayList<Location> stations_;
	ArrayList<Integer> travelTimes_;
	ArrayList<Transporter> vehicles_;

	public Line(ArrayList<Location> stations, ArrayList<Integer> travelTimes, int parId)
	{
		id_ = parId;
		vehicleCount_ = 0;
		averageWaitDelay_ = Integer.MAX_VALUE;
		this.stations_ = stations;
		this.travelTimes_ = travelTimes;

		for (Location station : stations)
			station.AddToLine(this);

		vehicles_ = new ArrayList<Transporter>();
	}

	// Travel time between station at index startId and station at index endId
	public int travelTime(int startId, int endId)
	{
		if (startId < 0 || endId > travelTimes_.size())
			return Integer.MAX_VALUE;

		if (endId == startId)
			return 0;

		int sum = 0;
		// Handle both directions
		for (int i = Math.min(startId, endId); i < Math.max(startId, endId); ++i)
			sum += travelTimes_.get(i);
		return sum;
	}

	// Travel time between two station on the line
	public int travelTime(Location start, Location end)
	{
		if (!stations_.contains(start) || !stations_.contains(end))
			return Integer.MAX_VALUE;
		return travelTime(stations_.indexOf(start), stations_.indexOf(end));
	}

	public void addVehicle(Transporter vehicle)
	{
		vehicleCount_ += 1;
		int sum = 0;
		for (int i = 0; i < travelTimes_.size(); ++i)
			sum += travelTimes_.get(i);
		averageWaitDelay_ = (sum << 1) / getVehicleNb();
		vehicles_.add(vehicle);
	}

	public int getId()
	{
		return id_;
	}

	public int getVehicleNb()
	{
		return vehicleCount_;
	}

	public ArrayList<Location> getStations()
	{
		return stations_;
	}
	
	public double getRatioOfWorkingVehicles()
	{
		double working = 0;
		for (Transporter vehi : vehicles_)
			if (!vehi.isBroken_)
				working++;
		return working / (double) vehicleCount_;
	}
}
