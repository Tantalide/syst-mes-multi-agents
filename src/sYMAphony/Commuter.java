package sYMAphony;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;


public class Commuter extends Agent {
	int currentStation_;
	int currentLine_;
	int destination_;
	int timeLived_;
	Transporter currentVehicle_;
	ArrayList<Point> path_;
	NetworkController network_;

	public Commuter(int start, int destination, Grid<Object> grid) {
		super(grid);
		currentStation_ = start;
		// Default value to enforce transit time on first update
		currentLine_ = -1;
		destination_ = destination;
		currentVehicle_ = null;
		network_ = null;
		timeLived_ = 0;
	}

	private class StationNode implements Comparable<StationNode>
	{
		public final Location station;
		public StationNode prev;
		public Integer lineToPrev;
		Integer dist;

		public StationNode(Location station) {
			this.station = station;
			prev = null;
			lineToPrev = -1;
			dist = Integer.MAX_VALUE;
		}

		@Override
		public int compareTo(StationNode o) 
		{
			return dist.compareTo(o.dist);
		}
	}

	public void findPath() {
		network_ = NetworkController.instance_;
		// Create nodes and queue for Dijkstra
		ArrayList<Location> stations = network_.stations_;
		StationNode[] nodes = new StationNode[NetworkController.STATION_COUNT];
		PriorityQueue<StationNode> stack = new PriorityQueue<StationNode>();

		// Initialize nodes from stations
		for (int i = 0; i < NetworkController.STATION_COUNT; ++i) {
			nodes[i] = new StationNode(stations.get(i));
			if (i == currentStation_)
			{
				nodes[i].dist = 0;
				stack.add(nodes[i]);
			}
			else
				nodes[i].dist = Integer.MAX_VALUE;
		}

		// Find path
		while (!stack.isEmpty())
		{
			StationNode node = stack.remove();
			if (node.station.getId() == destination_)
				break;

			for (Line line : node.station.lines_)
			{
				double waitTime = 0;
				if (line.getId() != node.lineToPrev)
					waitTime = (double) line.averageWaitDelay_ / (0.1d + line.getRatioOfWorkingVehicles());
				for (Location otherStation : line.getStations())
				{
					StationNode otherNode = nodes[otherStation.getId()];
					if (otherNode != node)
					{
						int totalWaitTime = node.dist + (int) waitTime + line.travelTime(node.station, otherStation);
						if (totalWaitTime < otherNode.dist) {
							otherNode.dist = totalWaitTime;
							otherNode.prev = node;
							otherNode.lineToPrev = line.getId();
							stack.add(otherNode);
						}
					}
				}
			}
		}

		path_ = new ArrayList<Point>();
		StationNode node = nodes[destination_];
		while (node.prev != null)
		{
			path_.add(new Point(node.station.getId(), node.lineToPrev));
			node = node.prev;
			if (path_.size() >= NetworkController.STATION_COUNT) // No reason to have more than station nodes, problem at generation so kill to hide
			{
				alive = false;
				break;
			}
		}

		Collections.reverse(path_);
	}

	@Override
	public void compute() {
		if (currentVehicle_ != null) 	// In a vehicle
		{
			Location targetStation = network_.stations_.get(path_.get(0).x);
			if (currentVehicle_.IsAtStation(targetStation)) // Arrived at the next path point
			{
				currentStation_ = targetStation.id_;
				if (currentStation_ == destination_)	// Arrived
				{
					currentVehicle_.passengers_.remove(this);
					alive = false;
					NetworkController network = NetworkController.instance_;
					network.totalTimeSpentInNetworkByTravelers_ += timeLived_;
					network.totalNumberOfTravelersThatPassedThroughNetwork_++;
					network.totalTimeSpentInNetworkByTravelersSinceLastDisplay_ += timeLived_;
					network.totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_++;
				}
				else
				{
					path_.remove(0);	// This part of the path is done

					if (currentVehicle_.getLine().getId() != path_.get(0).y) 	// Need to switch line
					{
						targetStation.travelers_.add(this);			// Now in the station
						currentVehicle_.passengers_.remove(this);
						currentVehicle_ = null;						// Exit the vehicle
					}
				}
			}
		}
		else 	// Waiting
		{
			ArrayList<Transporter> vehicles = network_.vehicles_;
			Location currentStation = network_.stations_.get(currentStation_);
			int targetLineId = path_.get(0).y;
			Location targetStation = network_.stations_.get(path_.get(0).x);
			for (Transporter vehicle : vehicles)
			{
				if (vehicle.IsAtStation(currentStation) && vehicle.line_.getId() == targetLineId)
					if (vehicle.getPassengerCount() < vehicle.getMaxCapacity())
						if (vehicle.isGoodDirectionToReachStation(targetStation))	// Avoid taking a vehicle going in the wrong direction
						{
							currentVehicle_ = vehicle;					// Get into vehicle
							currentVehicle_.passengers_.add(this);
							currentStation.travelers_.remove(this);		// Exit the station
							break;
						}
			}
		}
		timeLived_++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void implement()
	{
		if (!this.alive)
		{
			Context<Object> context = ContextUtils.getContext(this);
			context.remove(this);
		}
	}

}
