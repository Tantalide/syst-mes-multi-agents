package sYMAphony;

import java.awt.Color;

import repast.simphony.space.graph.RepastEdge;
import repast.simphony.visualizationOGL2D.EdgeStyleOGL2D;

public class MyEdgeStyle implements EdgeStyleOGL2D
{
	private static Color[] colors_ = {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.BLACK};

	@Override
	public int getLineWidth(RepastEdge<?> edge) {
		return 3;
	}

	@Override
	public Color getColor(RepastEdge<?> edge) {
		return colors_[(int) (edge.getWeight()) % colors_.length];
	}
}
