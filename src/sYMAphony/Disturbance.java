package sYMAphony;

import java.util.ArrayList;

public class Disturbance {
	public final String name;
	public final int delayCoeff;
	public final ArrayList<Line> affectedLines;
	public int duration;

	public Disturbance(String name, int delayCoeff, ArrayList<Line> affectedLines, int duration) {
		this.name = name;
		this.delayCoeff = delayCoeff;
		this.affectedLines = affectedLines;
		this.duration = duration;
	}
}
