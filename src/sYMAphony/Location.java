package sYMAphony;

import java.awt.Point;
import java.util.ArrayList;

import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

public class Location extends Agent
{
	public static final int UPDATE_DELAY = 10;
	ContinuousSpace<Object> space_;
	final Point pos_;
	final int id_;
	Boolean hasPannel_;
	ArrayList<Commuter> travelers_;
	ArrayList<Line> lines_;
	int timeTillPathUpdate_;

	public Location(ContinuousSpace<Object> space, Grid<Object> grid, Boolean hasPannel, int posX, int posY, int parId)
	{
		super(grid);
		id_ = parId;
		hasPannel_ = hasPannel;
		space_ = space;
		pos_ = new Point(posX, posY);
		travelers_ = new ArrayList<>();
		lines_ = new ArrayList<>();
		timeTillPathUpdate_ = UPDATE_DELAY;
	}

	public void AddToLine(Line line)
	{
		if (!lines_.contains(line))
			lines_.add(line);
	}

	@Override
	public void compute()
	{
		if (hasPannel_)
		{
			timeTillPathUpdate_--;
			if (timeTillPathUpdate_ == 0)
			{
				timeTillPathUpdate_ = UPDATE_DELAY;
				for (Commuter traveler : travelers_)
					traveler.findPath();
			}
		}
	}

	@Override
	public void implement() {}

	public int getId() {
		return id_;
	}

	public int getTravelerCount()
	{
		return travelers_.size();
	}

	int getPosX() {
		return pos_.x;
	}

	int getPosY() {
		return pos_.y;
	}

	public Boolean hasPannel() {
		return hasPannel_;
	}

	public ArrayList<Commuter> getTravelers() {
		return travelers_;
	}
}
