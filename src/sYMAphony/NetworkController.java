package sYMAphony;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.WrapAroundBorders;

public class NetworkController implements ContextBuilder<Object>  {
	public ArrayList<Location> stations_;
	ArrayList<Line> lines_;
	ArrayList<Transporter> vehicles_;

	Grid<Object> grid_;
	Context<Object> context_;
	public static Random random_ = new Random();
	public static NetworkController instance_ = null;

	public long totalTimeSpentInNetworkByTravelers_;
	public int totalNumberOfTravelersThatPassedThroughNetwork_;

	public long totalTimeSpentInNetworkByTravelersSinceLastDisplay_;
	public int totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_;
	public int nextDisplay_;
	public static final int DISPLAY_EVERY_X = 1000;
	
	
	public static final int STATION_COUNT = 15;
	public static final int LINE_COUNT = 6;
	public static final int TRAIN_COUNT = 18;
	public static final int PANEL_EACH_X = 3;
	public static final int GROWTH_DIVISOR = 10;

	// Display variables start
	JFrame currentGraphFrame_;
	JLabel currentGraphLabel_;
	double[] averageTimes_;
	double maxAverageTime_;
	int[] populationGrowths_;
	int maxPopulationGrowth_;
	double[] breakdownIntensities_;
	double maxBreakdownIntensity_;
	int[] travelerCounts_;
	int maxTravelerCount_;
	int currentGraphIndex_;
	// Display variables end

	public NetworkController() {
		stations_ = new ArrayList<>();
		lines_ = new ArrayList<>();
	}

	@SuppressWarnings("unchecked")
	public Context<Object> build(Context<Object> context) {
		context.setId("SYMAphony");

		GridFactory gridFactory = GridFactoryFinder.createGridFactory(null);
		grid_ = gridFactory.createGrid("grid", context,
				new GridBuilderParameters<Object>(new WrapAroundBorders(),
						new SimpleGridAdder<Object>(), true, 50, 50));
		
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder
				.createContinuousSpaceFactory(null);
		ContinuousSpace<Object> space = spaceFactory.createContinuousSpace(
				"space", context, new RandomCartesianAdder<Object>(),
				new repast.simphony.space.continuous.BouncyBorders(), 50, 50);

		NetworkBuilder<Object> netBuilder = new NetworkBuilder<Object>(
				"line network", context, false);
		netBuilder.buildNetwork();
		
		// Scheduler l'ajout des Travelers de facon plus ou moins al�atoire
		ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
		ScheduleParameters params = ScheduleParameters.createRepeating(1, 1);
		schedule.schedule(params, this, "globalControlLoop");

		if (instance_ == null)	// Don't add more hooks if the simulation was relaunched
		{
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				public void run() {
					System.out.println("Average time spent in network : " + (totalTimeSpentInNetworkByTravelers_ / totalNumberOfTravelersThatPassedThroughNetwork_));
					System.out.println("Number of travelers that passed through the network : " + totalNumberOfTravelersThatPassedThroughNetwork_);
				}
			}, "Shutdown-thread"));
		}
		
		ArrayList<Point> stationPositions = new ArrayList<Point>(STATION_COUNT);
		stationPositions.add(new Point(2, 2));
		stationPositions.add(new Point(6, 3));
		stationPositions.add(new Point(13, 2));
		stationPositions.add(new Point(1, 5));
		stationPositions.add(new Point(1, 9));
		stationPositions.add(new Point(3, 7));
		stationPositions.add(new Point(8, 7));
		stationPositions.add(new Point(11, 4));
		stationPositions.add(new Point(14, 8));
		stationPositions.add(new Point(15, 6));
		stationPositions.add(new Point(3, 12));
		stationPositions.add(new Point(8, 11));
		stationPositions.add(new Point(8, 13));
		stationPositions.add(new Point(14, 13));
		stationPositions.add(new Point(18, 11));
		stations_ = new ArrayList<Location>();
		for (int i = 0; i < STATION_COUNT; i++)
		{
			Location s = new Location(space, grid_, (i % PANEL_EACH_X) == 1, stationPositions.get(i).x * 2 + 7, stationPositions.get(i).y * 2 + 9, i);
			context.add(s);
			stations_.add(s);
		}

		ArrayList<ArrayList<Integer>> lineStations = new ArrayList<>(LINE_COUNT);
		lineStations.add(new ArrayList<>(Arrays.asList(0, 1, 6, 11, 13)));
		lineStations.add(new ArrayList<>(Arrays.asList(0, 2, 9, 14)));
		lineStations.add(new ArrayList<>(Arrays.asList(0, 3, 4, 10, 12, 13, 14)));
		lineStations.add(new ArrayList<>(Arrays.asList(4, 5, 6, 8, 9)));
		lineStations.add(new ArrayList<>(Arrays.asList(10, 5, 1, 7, 2)));
		lineStations.add(new ArrayList<>(Arrays.asList(7, 8, 14)));

		lines_ = new ArrayList<Line>();
		for (int i = 0; i < LINE_COUNT; ++i)
		{
			ArrayList<Location> stations = new ArrayList<Location>();
			ArrayList<Integer> travelTimes = new ArrayList<Integer>();
			ArrayList<Integer> stationIndex = lineStations.get(i);
			for (int j = 0; j < stationIndex.size(); ++j)
			{
				stations.add(stations_.get(stationIndex.get(j)));
				if (j > 0)
					travelTimes.add(8 - Math.abs(random_.nextInt()) % 6);
			}
			Line l = new Line(stations, travelTimes, i);
			lines_.add(l);
		}

		vehicles_ = new ArrayList<Transporter>();
		boolean isForth = true;
		for (int i = 0; i < TRAIN_COUNT; i++)
		{
			Line line = lines_.get(i % LINE_COUNT);
			int lineLength = line.getStations().size();
			int count = line.getVehicleNb();
			Transporter t = new Transporter(grid_, space, line, count % lineLength, isForth);
			isForth = !isForth;
			line.addVehicle(t);
			context.add(t);
			vehicles_.add(t);
		}

		// Placer les trains
		for (Transporter t : vehicles_)
		{
			space.moveTo(t, t.getPosX(), t.getPosY());
			grid_.moveTo(t, t.getPosX(), t.getPosY());
		}

		// Placer les stations
		for (Location s : stations_)
		{
			grid_.moveTo(s, s.getPosX(), s.getPosY());
			space.moveTo(s, s.getPosX(), s.getPosY());
		}

		Network<Object> net = (Network<Object>) context.getProjection("line network");
		for (Line line : lines_)
		{
			int length = line.getStations().size() - 1;
			for (int i = 0; i < length; ++i) {
				RepastEdge<Object> newEdge = new RepastEdge<Object>(line.getStations().get(i), line.getStations().get(i + 1), false);
				newEdge.setWeight(line.id_);
				net.addEdge(newEdge);				
			}
		}

		instance_ = this;			// Used to simulate singleton
		totalTimeSpentInNetworkByTravelers_ = 0;
		totalNumberOfTravelersThatPassedThroughNetwork_ = 0;
		totalTimeSpentInNetworkByTravelersSinceLastDisplay_ = 0;
		totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_ = 0;
		nextDisplay_ = DISPLAY_EVERY_X;
		averageTimes_ = new double[25600];
		populationGrowths_ = new int[25600];
		breakdownIntensities_ = new double[25600];
		travelerCounts_ = new int[25600];
		maxAverageTime_ = 0;
		maxPopulationGrowth_ = 0;
		maxBreakdownIntensity_ = 0;
		maxTravelerCount_ = 0;
		currentGraphIndex_ = 0;
		currentGraphFrame_ = null;
		context_ = context;

		return context;
	}

	public void globalControlLoop()
	{	
		int growth = (Integer) RunEnvironment.getInstance().getParameters().getValue("populationGrowth");

		for (int i = 0; i < growth; i++)
		{
			if (Math.abs(random_.nextInt()) % GROWTH_DIVISOR == 0)
			{
				int start = Math.abs(random_.nextInt()) % STATION_COUNT;
				int end = Math.abs(random_.nextInt()) % STATION_COUNT;
				if (start != end)
				{
					Commuter newTraveler = new Commuter(start, end, grid_);
					newTraveler.findPath();
					stations_.get(start).getTravelers().add(newTraveler);
					context_.add(newTraveler);
				}
			}
		}

		nextDisplay_--;
		if (nextDisplay_ == 0)
		{
			nextDisplay_ = DISPLAY_EVERY_X;
			double avgTime = 0;
			if (totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_ > 5) // With less data is too random
				avgTime = (double) totalTimeSpentInNetworkByTravelersSinceLastDisplay_ / (double) totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_;
			totalTimeSpentInNetworkByTravelersSinceLastDisplay_ = 0;
			totalNumberOfTravelersThatPassedThroughNetworkSinceLastDisplay_ = 0;

			averageTimes_[currentGraphIndex_] = avgTime;
			populationGrowths_[currentGraphIndex_] = (Integer) RunEnvironment.getInstance().getParameters().getValue("populationGrowth");
			int repairTime =  (Integer) RunEnvironment.getInstance().getParameters().getValue("vehicleBreakdownRepairTime");
			int breakInX = (Integer) RunEnvironment.getInstance().getParameters().getValue("chanceInXForVehicleBreakdown");
			breakdownIntensities_[currentGraphIndex_] = (double) repairTime / (double) breakInX;

			int travelerCount = 0;
			for (Location stat : stations_)
				travelerCount += stat.getTravelerCount();
			travelerCounts_[currentGraphIndex_] = travelerCount;

			if (avgTime > maxAverageTime_)
				maxAverageTime_ = avgTime;
			if (populationGrowths_[currentGraphIndex_] > maxPopulationGrowth_)
				maxPopulationGrowth_ = populationGrowths_[currentGraphIndex_];
			if (breakdownIntensities_[currentGraphIndex_] > maxBreakdownIntensity_)
				maxBreakdownIntensity_ = breakdownIntensities_[currentGraphIndex_];
			if (travelerCount > maxTravelerCount_)
				maxTravelerCount_ = travelerCount;

			currentGraphIndex_++;
			if (currentGraphIndex_ == 25600)
			{
				currentGraphIndex_ = 0;
				currentGraphFrame_.dispose();
				currentGraphFrame_ = null;
				averageTimes_[0] = averageTimes_[25599];
				populationGrowths_[0] = populationGrowths_[25599];
				breakdownIntensities_[0] = breakdownIntensities_[25599];
				travelerCounts_[0] = travelerCounts_[25599];
			}

			BufferedImage img = new BufferedImage(560, 480, BufferedImage.TYPE_3BYTE_BGR);
			if (currentGraphFrame_ == null && currentGraphIndex_ != 1)	// Graph closed
			{
				averageTimes_ = new double[25600];
				populationGrowths_ = new int[25600];
				breakdownIntensities_ = new double[25600];
				travelerCounts_ = new int[25600];
				maxAverageTime_ = 0;
				maxPopulationGrowth_ = 0;
				maxBreakdownIntensity_ = 0;
				maxTravelerCount_ = 0;
				currentGraphIndex_ = 0;
			}
			else
			{
				Graphics2D g2d = img.createGraphics();
				g2d.setBackground(Color.WHITE);
				g2d.clearRect(0, 0, 560, 480);
				g2d.setStroke(new BasicStroke(2));

				g2d.setColor(Color.BLUE);
				int oldX = 0;
				int oldY = 160 - (int) (averageTimes_[0] * 140.d / maxAverageTime_);
				int xMult = Math.max(128, currentGraphIndex_);
				int increment = Math.max(1, currentGraphIndex_ / 128);
				for (int i = increment; i < currentGraphIndex_; i += increment)
				{
					double sumVal = 0;
					for (int j = 0; j < increment; j++)
						sumVal += averageTimes_[i - j];
					int x = (int) ((double) i * 400.d) / xMult;
					int y = 160 - (int) (sumVal / increment * 140.d / maxAverageTime_);
					g2d.drawLine(oldX, oldY, x, (oldY + y) / 2);
					oldX = x;
					oldY = (oldY + y) / 2;
				}
				g2d.drawString("Avg Wait Time : " + (int) averageTimes_[currentGraphIndex_ - 1], oldX + 20, oldY);

				g2d.setColor(Color.GREEN);
				oldX = 0;
				oldY = 480 - (int) (populationGrowths_[0] * 60.d / maxPopulationGrowth_);
				for (int i = increment; i < currentGraphIndex_; i += increment)
				{
					double sumVal = 0;
					for (int j = 0; j < increment; j++)
						sumVal += populationGrowths_[i - j];
					int x = (int) ((double) i * 400.d) / xMult;
					int y = 480 - (int) (sumVal / increment * 60.d / maxPopulationGrowth_);
					g2d.drawLine(oldX, oldY, x, (oldY + y) / 2);
					oldX = x;
					oldY = (oldY + y) / 2;
				}
				g2d.drawString("Growth Param : " + populationGrowths_[currentGraphIndex_ - 1], oldX + 20, oldY);

				g2d.setColor(Color.RED);
				oldX = 0;
				oldY = 400 - (int) (breakdownIntensities_[0] * 60.d / maxBreakdownIntensity_);
				for (int i = increment; i < currentGraphIndex_; i += increment)
				{
					double sumVal = 0;
					for (int j = 0; j < increment; j++)
						sumVal += breakdownIntensities_[i - j];
					int x = (int) ((double) i * 400.d) / xMult;
					int y = 400 - (int) (sumVal / increment * 60.d / maxBreakdownIntensity_);
					g2d.drawLine(oldX, oldY, x, (oldY + y) / 2);
					oldX = x;
					oldY = (oldY + y) / 2;
				}
				g2d.drawString("Disruption Param : " + (int) (breakdownIntensities_[currentGraphIndex_ - 1] * 100.d), oldX + 20, oldY);

				g2d.setColor(Color.BLACK);
				oldX = 0;
				oldY = 320 - (int) (travelerCounts_[0] * 140.d / maxTravelerCount_);
				for (int i = increment; i < currentGraphIndex_; i += increment)
				{
					double sumVal = 0;
					for (int j = 0; j < increment; j++)
						sumVal += travelerCounts_[i - j];
					int x = (int) ((double) i * 400.d) / xMult;
					int y = 320 - (int) (sumVal / increment * 140.d / maxTravelerCount_);
					g2d.drawLine(oldX, oldY, x, (oldY + y) / 2);
					oldX = x;
					oldY = (oldY + y) / 2;
				}
				g2d.drawString("Traveler count : " + travelerCounts_[currentGraphIndex_ - 1], oldX + 20, oldY);
				if (currentGraphFrame_ == null)
				{
					currentGraphFrame_ = new JFrame("Network data display");
					currentGraphLabel_ = new JLabel();
					currentGraphFrame_.add(currentGraphLabel_);
					currentGraphFrame_.setMinimumSize(new Dimension(560, 540));
					currentGraphFrame_.setVisible(true);
					currentGraphFrame_.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
					currentGraphFrame_.addWindowListener(new WindowListener() {

						@Override
						public void windowOpened(WindowEvent arg0) {}

						@Override
						public void windowIconified(WindowEvent arg0) {}

						@Override
						public void windowDeiconified(WindowEvent arg0) {}

						@Override
						public void windowDeactivated(WindowEvent arg0) {}

						@Override
						public void windowClosing(WindowEvent arg0) {}

						@Override
						public void windowClosed(WindowEvent arg0) {currentGraphFrame_ = null;}

						@Override
						public void windowActivated(WindowEvent arg0) {}
					});
				}
				currentGraphLabel_.setIcon(new ImageIcon(img));
			}
		}
	}
}
