package sYMAphony;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

import java.awt.Point;
import java.util.ArrayList;

public class Transporter extends Agent
{
	ContinuousSpace<Object> space_;
	boolean moved_;
	Point pos_;
	final int maxCapacity_ = 20;
	ArrayList<Commuter> passengers_;
	Boolean isForth_;
	Line line_;
	int linePosition_;
	int timeToStation_;

	public boolean isBroken_;
	int timeTillRepair_;

	public Transporter(Grid<Object> grid, ContinuousSpace<Object> space, Line line, int linePosition, Boolean isForth)
	{
		super(grid);
		space_ = space;
		moved_ = false;
		pos_ = new Point(line.getStations().get(linePosition).getPosX(), line.getStations().get(linePosition).getPosY());
		passengers_ = new ArrayList<Commuter>();
		isForth_ = isForth;
		line_ = line;
		linePosition_ = linePosition;
		timeToStation_ = line.travelTime(linePosition, NextStationPosition());
		isBroken_ = false;
	}

	@Override
	public void compute()
	{
		// Vehicles may break down from time to time
		if (isBroken_)
		{
			timeTillRepair_--;
			if (timeTillRepair_ > 0)
				return;
			isBroken_ = false;
		}
		int chanceInX = (Integer) RunEnvironment.getInstance().getParameters().getValue("chanceInXForVehicleBreakdown");
		if (Math.abs(NetworkController.random_.nextInt()) % chanceInX == 0)
		{
			isBroken_ = true;
			timeTillRepair_ = (Integer) RunEnvironment.getInstance().getParameters().getValue("vehicleBreakdownRepairTime");
		}

		// get the grid location of the next station
		int nextPosition = NextStationPosition();
		GridPoint targetPos = grid.getLocation(line_.getStations().get(nextPosition));

		if (timeToStation_ == 0) {
			timeToStation_ = line_.travelTime(linePosition_, nextPosition);
			if (nextPosition == 0 || nextPosition == line_.getStations().size() - 1)
				isForth_ = !isForth_;
			linePosition_ = nextPosition;
		}
		else
		{
			if (!targetPos.equals(grid.getLocation(this)))
			{
				NdPoint point = space_.getLocation(this);
				NdPoint otherPoint = new NdPoint(targetPos.getX(), targetPos.getY());
				double angle = SpatialMath.calcAngleFor2DMovement(space_, point, otherPoint);
				double dist = Math.sqrt(Math.pow(otherPoint.getX() - point.getX(), 2) + Math.pow(otherPoint.getY() - point.getY(), 2));
				space_.moveByVector(this, dist / timeToStation_, angle, 0);
				point = space_.getLocation(this);
				grid.moveTo(this, (int) point.getX(), (int) point.getY());

				moved_ = true;
			}
			--timeToStation_;
		}
	}

	@Override
	public void implement() {}

	public Boolean IsAtStation()
	{
		return (timeToStation_ == 0);
	}

	public Boolean IsAtStation(Location station)
	{
		return (IsAtStation() && (line_.getStations().get(NextStationPosition()) == station));
	}

	public Boolean IsAtStation(int position)
	{
		return (IsAtStation() && (linePosition_ == position));
	}

	public int getPassengerCount()
	{
		return passengers_.size();
	}

	public int NextStationPosition()
	{
		// if the vehicle can move forward
		if ((isForth_ && linePosition_ + 1 < line_.getStations().size()) || linePosition_ == 0)
			return linePosition_ + 1;
		return linePosition_ - 1;
	}

	public boolean isGoodDirectionToReachStation(Location station)
	{
		int targetPos = line_.getStations().indexOf(station);
		int currentPos = NextStationPosition();					// Since this is called when at a station, the NextStationPosition is required
		if (currentPos == 0 || currentPos == (line_.getStations().size() - 1))
			return true;										// Always take on limits
		if (targetPos >= currentPos && isForth_)
			return true;
		if (targetPos <= currentPos && !isForth_)
			return true;
		return false;
	}

	int getPosX() {
		return pos_.x;
	}

	int getPosY() {
		return pos_.y;
	}

	public int getMaxCapacity() {
		return maxCapacity_;
	}

	public Line getLine() {
		return line_;
	}
}
